##### 01- Build app
FROM node:8.4.0-wheezy AS node
WORKDIR /app
COPY package.json ./
RUN npm install --production
COPY . .

##### 02- Run NGINX using build from step 01

FROM nginx:alpine

EXPOSE 9005

COPY --from=node /app/dist /usr/share/nginx/html