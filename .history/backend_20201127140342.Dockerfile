##### 01- Build app
FROM node:8.4.0-wheezy AS node
WORKDIR /app
COPY package.json ./
RUN npm install --production
COPY . .

##### 02- Run NGINX using build from step 01
EXPOSE 8081

CMD [ "npm","start"]

CMD [ "server.js" ]
