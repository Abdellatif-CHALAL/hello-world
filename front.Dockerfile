
##### 01- Build app
FROM node:8.4.0-wheezy AS node
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

##### 02- Run NGINX using build from step 01

FROM nginx:alpine

COPY --from=node /app/dist /usr/share/nginx/html
